#include "../tri_list.h"
#include <vector>
#include <ranges>
#include <cassert>

template<typename T, typename tri_t>
std::vector<T> from_tri(tri_t& tri) {
    auto view = tri.template range_over<T>();
	return std::vector<T>(std::ranges::begin(view), std::ranges::end(view));
}

int main() {
    tri_list<int, double, bool> l({1, 1.0, true});
    
    auto fi = [](int x) { return x + 1; };
    auto fb = [](bool x) { return !x; };

    assert((from_tri<int>(l) == std::vector{1}));
    assert((from_tri<bool>(l) == std::vector{true}));
    
    l.modify_only<int>(fi);
    assert((from_tri<int>(l) == std::vector{2}));
    assert((from_tri<bool>(l) == std::vector{true}));

    l.modify_only<bool>(fb);
    assert((from_tri<int>(l) == std::vector{2}));
    assert((from_tri<bool>(l) == std::vector{false}));

    l.reset<int>();
    assert((from_tri<int>(l) == std::vector{1}));
    assert((from_tri<bool>(l) == std::vector{false}));
    
    l.reset<int>();
    assert((from_tri<int>(l) == std::vector{1}));
    assert((from_tri<bool>(l) == std::vector{false}));

    l.reset<bool>();
    assert((from_tri<int>(l) == std::vector{1}));
    assert((from_tri<bool>(l) == std::vector{true}));

    l.modify_only<bool>(fb);
    l.modify_only<bool>(fb);
    l.modify_only<int>(fi);
    l.modify_only<int>(fi);
    l.modify_only<int>(fi);
    assert((from_tri<int>(l) == std::vector{4}));
    assert((from_tri<bool>(l) == std::vector{true}));

    l.reset<bool>();
    assert((from_tri<int>(l) == std::vector{4}));
    assert((from_tri<bool>(l) == std::vector{true}));
    
    l.reset<int>();
    assert((from_tri<int>(l) == std::vector{1}));
    assert((from_tri<bool>(l) == std::vector{true}));
}
