#include "../tri_list.h"

struct A {
    int operator()(int x) {
        return x;
    }
} a;

int main() {
    tri_list<int, double, bool> l;
    l.modify_only<int>(compose<int>(a, a));
}
